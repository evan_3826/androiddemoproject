package lab.infoworks.starter.ui.fragment.PaymentFlow.PaymentDetails;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lab.infoworks.starter.R;
import lab.infoworks.starter.ui.Model.PaymentMethod;

public class PaymentDetailsFragment extends Fragment {

    @BindView(R.id.payMethodRecyclerView)
    RecyclerView payMethodRecyclerView;
    private List<PaymentMethod> paymentMethodList=new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_details, container, false);
        ButterKnife.bind(this, view);
        loadContents();
        return view;
    }

    private void loadContents() {
        payMethodRecyclerView.setAdapter(new PaymentDetailsRecyclerAdapter(paymentMethodList));
        payMethodRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),RecyclerView.HORIZONTAL,false));
        addPaymentMethodOptions();
    }

    private void addPaymentMethodOptions() {
        paymentMethodList.add(new PaymentMethod("Cash",R.drawable.cash_image));
        paymentMethodList.add(new PaymentMethod("Bkash",R.drawable.bkash_image));
        paymentMethodList.add(new PaymentMethod("Nagad",R.drawable.nagad_image));
    }
}
