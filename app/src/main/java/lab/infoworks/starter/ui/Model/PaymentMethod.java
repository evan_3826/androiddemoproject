package lab.infoworks.starter.ui.Model;

public class PaymentMethod {

    private String paymentMethodTitleText;
    private int paymentMethodLogoImage;

    public String getPaymentMethodTitleText() {
        return paymentMethodTitleText;
    }

    public void setPaymentMethodTitleText(String paymentMethodTitleText) {
        this.paymentMethodTitleText = paymentMethodTitleText;
    }

    public int getPaymentMethodLogoImage() {
        return paymentMethodLogoImage;
    }

    public void setPaymentMethodLogoImage(int paymentMethodLogoImage) {
        this.paymentMethodLogoImage = paymentMethodLogoImage;
    }

    public PaymentMethod(String paymentMethodTitleText, int paymentMethodLogoImage) {
        this.paymentMethodTitleText = paymentMethodTitleText;
        this.paymentMethodLogoImage = paymentMethodLogoImage;
    }
}
