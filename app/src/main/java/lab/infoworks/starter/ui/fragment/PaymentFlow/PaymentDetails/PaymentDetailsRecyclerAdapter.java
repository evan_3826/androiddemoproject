package lab.infoworks.starter.ui.fragment.PaymentFlow.PaymentDetails;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lab.infoworks.starter.R;
import lab.infoworks.starter.ui.Model.PaymentMethod;

public class PaymentDetailsRecyclerAdapter extends RecyclerView.Adapter<PaymentDetailsRecyclerAdapter.ViewHolder>{

    private List<PaymentMethod> paymentMethodList;

    public PaymentDetailsRecyclerAdapter(List<PaymentMethod> paymentMethodList) {
        this.paymentMethodList = paymentMethodList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_method_list,null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PaymentMethod paymentMethod=paymentMethodList.get(position);

        holder.paymentMethodLogoImage.setImageResource(paymentMethod.getPaymentMethodLogoImage());
        holder.paymentMethodTitleText.setText(paymentMethod.getPaymentMethodTitleText());
    }

    @Override
    public int getItemCount() {
        return paymentMethodList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.paymentMethodLogoImage)
        ImageView paymentMethodLogoImage;
        @BindView(R.id.paymentMethodTitleText)
        TextView paymentMethodTitleText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
