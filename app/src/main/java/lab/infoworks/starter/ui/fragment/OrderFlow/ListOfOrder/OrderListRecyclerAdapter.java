package lab.infoworks.starter.ui.fragment.OrderFlow.ListOfOrder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lab.infoworks.starter.R;
import lab.infoworks.starter.ui.Model.OrderInfo;

public class OrderListRecyclerAdapter extends RecyclerView.Adapter<OrderListRecyclerAdapter.ViewHolder> {

    private List<OrderInfo> orderInfos;

    public OrderListRecyclerAdapter(List<OrderInfo> orderInfos) {
        this.orderInfos = orderInfos;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.order_info_list,null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderInfo orderInfo=orderInfos.get(position);
        holder.productNameTextView.setText(orderInfo.getProductNameTextView());
        holder.merchantNameTextView.setText(orderInfo.getMerchantNameTextView());
        holder.departedTimingTextView.setText(orderInfo.getDepartedTimingTextView());
        holder.orderIDTextView.setText(orderInfo.getOrderIDTextView());
        holder.collectAbleMoneyTextView.setText(orderInfo.getCollectAbleMoneyTextView());
        holder.deliveryTypeTextView.setText(orderInfo.getDeliveryTypeTextView());
    }

    @Override
    public int getItemCount() {
        return orderInfos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.productNameTextView)
        TextView productNameTextView;
        @BindView(R.id.merchantNameTextView)
        TextView merchantNameTextView;
        @BindView(R.id.departedTimingTextView)
        TextView departedTimingTextView;
        @BindView(R.id.orderIDTextView)
        TextView orderIDTextView;
        @BindView(R.id.collectAbleMoneyTextView)
        TextView collectAbleMoneyTextView;
        @BindView(R.id.deliveryTypeTextView)
        TextView deliveryTypeTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
