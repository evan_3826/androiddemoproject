package lab.infoworks.starter.ui.fragment.OrderFlow.ListOfOrder;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lab.infoworks.starter.R;
import lab.infoworks.starter.ui.Model.OrderInfo;

public class OrderListFragment extends Fragment {

    private List<OrderInfo> orderInfos = new ArrayList<>();

    @BindView(R.id.orderListRecyclerView)
    RecyclerView orderListRecyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_list, container, false);
        ButterKnife.bind(this, view);
        loadContents();
        return view;
    }

    private void loadContents() {

        orderListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        orderListRecyclerView.setAdapter(new OrderListRecyclerAdapter(orderInfos));
        addOrder();
    }

    private void addOrder() {
        orderInfos.add(new OrderInfo("JBL speaker", "Daily Electronics", "Departed 20 minutes ago"
                , "123645", "560" + " BDT", "COD"));
        orderInfos.add(new OrderInfo("JBL headphone", "Pickaboo BD", "Not ready yet "
                , "", "560" + " BDT", "COD"));
        orderInfos.add(new OrderInfo("Document", "4140 Parker Rd. Allentown, New Dhanmondi", ""
                , "123645", "560" + " BDT", "COD"));
    }
}
