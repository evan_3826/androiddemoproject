package lab.infoworks.starter.ui.Model;

public class OrderInfo {

    private String productNameTextView, merchantNameTextView, departedTimingTextView,
            orderIDTextView, collectAbleMoneyTextView, deliveryTypeTextView;

    public String getProductNameTextView() {
        return productNameTextView;
    }

    public void setProductNameTextView(String productNameTextView) {
        this.productNameTextView = productNameTextView;
    }

    public String getMerchantNameTextView() {
        return merchantNameTextView;
    }

    public void setMerchantNameTextView(String merchantNameTextView) {
        this.merchantNameTextView = merchantNameTextView;
    }

    public String getDepartedTimingTextView() {
        return departedTimingTextView;
    }

    public void setDepartedTimingTextView(String departedTimingTextView) {
        this.departedTimingTextView = departedTimingTextView;
    }

    public String getOrderIDTextView() {
        return orderIDTextView;
    }

    public void setOrderIDTextView(String orderIDTextView) {
        this.orderIDTextView = orderIDTextView;
    }

    public String getCollectAbleMoneyTextView() {
        return collectAbleMoneyTextView;
    }

    public void setCollectAbleMoneyTextView(String collectAbleMoneyTextView) {
        this.collectAbleMoneyTextView = collectAbleMoneyTextView;
    }

    public String getDeliveryTypeTextView() {
        return deliveryTypeTextView;
    }

    public void setDeliveryTypeTextView(String deliveryTypeTextView) {
        this.deliveryTypeTextView = deliveryTypeTextView;
    }

    public OrderInfo(String productNameTextView, String merchantNameTextView, String departedTimingTextView, String orderIDTextView, String collectAbleMoneyTextView, String deliveryTypeTextView) {
        this.productNameTextView = productNameTextView;
        this.merchantNameTextView = merchantNameTextView;
        this.departedTimingTextView = departedTimingTextView;
        this.orderIDTextView = orderIDTextView;
        this.collectAbleMoneyTextView = collectAbleMoneyTextView;
        this.deliveryTypeTextView = deliveryTypeTextView;
    }
}
