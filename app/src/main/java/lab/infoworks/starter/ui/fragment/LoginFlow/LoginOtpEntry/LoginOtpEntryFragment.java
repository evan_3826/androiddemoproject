package lab.infoworks.starter.ui.fragment.LoginFlow.LoginOtpEntry;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.lamonjush.pinentryedittext.PinEntryEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lab.infoworks.starter.R;

public class LoginOtpEntryFragment extends Fragment {

    @BindView(R.id.otpView)
    PinEntryEditText otpView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_otp_entry,container,false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        autoSoftKeypad();
    }

    private void autoSoftKeypad() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        InputMethodManager imgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imgr.showSoftInput(otpView, InputMethodManager.SHOW_IMPLICIT);
    }

    @OnClick(R.id.backArrowImage)
    void onBackArrowImageClicked(){
        getActivity().onBackPressed();
    }

    @OnClick(R.id.rightArrowImageView)
    void onRightArrowImageViewClicked(){
        NavHostFragment.findNavController(this).navigate(R.id.action_loginOtpEntryFragment_to_userAgreementFragment);
    }
}
