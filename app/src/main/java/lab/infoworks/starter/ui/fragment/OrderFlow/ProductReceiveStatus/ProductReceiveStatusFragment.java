package lab.infoworks.starter.ui.fragment.OrderFlow.ProductReceiveStatus;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import butterknife.ButterKnife;
import lab.infoworks.starter.R;

public class ProductReceiveStatusFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_product_receive_confirmation,container,false);
        ButterKnife.bind(this,view);
        return view;
    }
}
