package lab.infoworks.starter.ui.fragment.LoginFlow.UserAgreement;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;
import lab.infoworks.starter.R;

public class UserAgreementFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_user_agreement,container,false);
        ButterKnife.bind(this,view);
        return view;
    }

    @OnClick(R.id.declineButton)
    void onDeclineButtonClicked(){
        NavHostFragment.findNavController(this).navigate(R.id.action_userAgreementFragment_to_loginIntroFragment);
    }
}
