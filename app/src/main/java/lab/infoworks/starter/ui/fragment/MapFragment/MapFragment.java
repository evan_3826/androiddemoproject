package lab.infoworks.starter.ui.fragment.MapFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lab.infoworks.starter.R;

public class MapFragment extends Fragment {

    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_map,container,false);
        ButterKnife.bind(this,view);
        return view;
    }

    @OnClick(R.id.menuImageView)
    void onMenuImageClicked(){
        drawerLayout.openDrawer(GravityCompat.START);
    }
}
