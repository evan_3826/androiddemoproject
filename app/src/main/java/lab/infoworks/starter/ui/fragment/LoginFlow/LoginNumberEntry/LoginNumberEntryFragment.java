package lab.infoworks.starter.ui.fragment.LoginFlow.LoginNumberEntry;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lab.infoworks.starter.R;

public class LoginNumberEntryFragment extends Fragment {

    @BindView(R.id.phoneNumberEntry)
    EditText phoneNumberEntry;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_number_entry,container,false);
        ButterKnife.bind(this,view);
        phoneNumberEntry.requestFocus();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        autoSoftKeypad();
    }

    private void autoSoftKeypad() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(phoneNumberEntry, InputMethodManager.SHOW_IMPLICIT);
    }

    @OnClick(R.id.backArrowImage)
    void onBackArrowImageClicked(){
        getActivity().onBackPressed();
    }

    @OnClick(R.id.rightArrowImageView)
    void onRightArrowImageViewClicked(){
//        String phoneNumber=phoneNumberEntry.getText().toString();
        NavHostFragment.findNavController(this).navigate(R.id.action_loginNumberEntryFragment_to_loginOtpEntryFragment);
    }
}
