package lab.infoworks.starter.dev;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import lab.infoworks.starter.R;
import lab.infoworks.starter.ui.fragment.MapFragment.MapFragment;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        MapFragment fragment = new MapFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }
}